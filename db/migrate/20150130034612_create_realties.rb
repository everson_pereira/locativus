class CreateRealties < ActiveRecord::Migration
  def change
    create_table :realties do |t|
      t.references :realty_type, index: true
      t.references :purpose, index: true
      t.references :address, index: true
      t.string :number
      t.string :complement
      t.string :short_description
      t.string :description
      t.integer :bedrooms
      t.integer :util_area
      t.integer :total_area
      t.decimal :value, precision: 8, scale: 2
      t.decimal :condominium, precision: 8, scale: 2
      t.integer :bathrooms
      t.integer :garage
      t.integer :realstate_id

      t.timestamps
    end
  end
end
