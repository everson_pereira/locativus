class CreateNeighborhoods < ActiveRecord::Migration
  def change
    create_table :neighborhoods do |t|
      t.string :description
      t.references :city, index: true

      t.timestamps
    end
  end
end
