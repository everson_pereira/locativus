class CreateRealStates < ActiveRecord::Migration
  def change
    create_table :real_states do |t|
      t.string :name
      t.string :host

      t.timestamps
    end
  end
end
