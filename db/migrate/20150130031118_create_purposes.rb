class CreatePurposes < ActiveRecord::Migration
  def change
    create_table :purposes do |t|
      t.string :description

      t.timestamps
    end
  end
end
