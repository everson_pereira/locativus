require 'test_helper'

class RealtiesControllerTest < ActionController::TestCase
  setup do
    @realty = realties(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:realties)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create realty" do
    assert_difference('Realty.count') do
      post :create, realty: { address_id: @realty.address_id, bathrooms: @realty.bathrooms, bedrooms: @realty.bedrooms, complement: @realty.complement, condominium: @realty.condominium, description: @realty.description, garage: @realty.garage, number: @realty.number, purpose_id: @realty.purpose_id, realstate_id: @realty.realstate_id, realty_type_id: @realty.realty_type_id, short_description: @realty.short_description, total_area: @realty.total_area, util_area: @realty.util_area, value: @realty.value }
    end

    assert_redirected_to realty_path(assigns(:realty))
  end

  test "should show realty" do
    get :show, id: @realty
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @realty
    assert_response :success
  end

  test "should update realty" do
    patch :update, id: @realty, realty: { address_id: @realty.address_id, bathrooms: @realty.bathrooms, bedrooms: @realty.bedrooms, complement: @realty.complement, condominium: @realty.condominium, description: @realty.description, garage: @realty.garage, number: @realty.number, purpose_id: @realty.purpose_id, realstate_id: @realty.realstate_id, realty_type_id: @realty.realty_type_id, short_description: @realty.short_description, total_area: @realty.total_area, util_area: @realty.util_area, value: @realty.value }
    assert_redirected_to realty_path(assigns(:realty))
  end

  test "should destroy realty" do
    assert_difference('Realty.count', -1) do
      delete :destroy, id: @realty
    end

    assert_redirected_to realties_path
  end
end
