class SiteController < ApplicationController
  
  authorize_resource :class => false
  
  def index
    @realties = Realty.all
  end
  
end
