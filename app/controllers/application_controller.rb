class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  #set_current_tenant_by_subdomain(:real_state, :host)
  set_current_tenant_through_filter 
  before_filter :discover_tenant_from_domain_name
  def discover_tenant_from_domain_name
    set_current_tenant RealState.find_by host: request.subdomains.last
    unless current_tenant
      if user_signed_in?
        set_current_tenant RealState.find_by id: current_user.realstate_id
      end   
    end     
  end
  
  layout :layout_by_resource
  def layout_by_resource
    if current_tenant
      "realstate"
    else
      "application"
    end
  end
  
  #Redirects to login for secure resources
  # rescue_from CanCan::AccessDenied do |exception|
# 
    # if user_signed_in?
      # flash[:error] = "Not authorized to view this page"
      # session[:user_return_to] = nil
      # redirect_to root_url
# 
    # else              
      # flash[:error] = "You must first login to view this page"
      # session[:user_return_to] = request.url
      # redirect_to "/users/sign_in"
    # end 
# 
  # end 
    
  def after_sign_out_path_for(resource_or_scope)
    scope = Devise::Mapping.find_scope!(resource_or_scope)    
  	if scope == :user
      respond_to?(:root_path) ? root_path : "/"    
    elsif scope == :admin 
      respond_to?(:root_path) ? root_path + "admin/index" : "/admin/index"
    end
  end
  
  def after_sign_in_path_for(resource_or_scope)
    scope = Devise::Mapping.find_scope!(resource_or_scope)    
    if scope == :user
      respond_to?(:root_path) ? root_path + "user/index" : "/user/index"    
    elsif scope == :admin 
      respond_to?(:root_path) ? root_path + "admin/index" : "/admin/index"
    end
  end

  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << :realstate_id
  end
  
end
