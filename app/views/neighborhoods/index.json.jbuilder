json.array!(@neighborhoods) do |neighborhood|
  json.extract! neighborhood, :id, :description, :city_id
  json.url neighborhood_url(neighborhood, format: :json)
end
