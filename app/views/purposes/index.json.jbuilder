json.array!(@purposes) do |purpose|
  json.extract! purpose, :id, :description
  json.url purpose_url(purpose, format: :json)
end
