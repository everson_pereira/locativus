json.array!(@realties) do |realty|
  json.extract! realty, :id, :realty_type_id, :purpose_id, :address_id, :number, :complement, :short_description, :description, :bedrooms, :util_area, :total_area, :value, :condominium, :bathrooms, :garage, :realstate_id
  json.url realty_url(realty, format: :json)
end
