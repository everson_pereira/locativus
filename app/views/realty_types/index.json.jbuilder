json.array!(@realty_types) do |realty_type|
  json.extract! realty_type, :id, :description
  json.url realty_type_url(realty_type, format: :json)
end
