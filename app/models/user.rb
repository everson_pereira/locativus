class User < ActiveRecord::Base
  rolify
  
  acts_as_tenant(:realstate)
  validates_uniqueness_to_tenant :email
  
  # Include default devise modules. Others available are:
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable, :recoverable, :rememberable, :trackable#, :validatable 
  
  after_create :assign_default_role
  def assign_default_role
    add_role(:member)
  end

end
