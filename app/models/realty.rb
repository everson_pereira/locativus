class Realty < ActiveRecord::Base
  belongs_to :realty_type
  belongs_to :purpose
  belongs_to :address
  
  accepts_nested_attributes_for :address
  
  acts_as_tenant(:realstate)
end
