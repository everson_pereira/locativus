class Address < ActiveRecord::Base
  belongs_to :neighborhood
  
  accepts_nested_attributes_for :neighborhood
  
end
