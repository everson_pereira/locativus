class City < ActiveRecord::Base
  belongs_to :state
  
  accepts_nested_attributes_for :state
  
end
